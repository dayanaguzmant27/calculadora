let numa;
let numb;
let operacion;


function iniciar(){

    let resultado=document.getElementById("resultado");
    let uno=document.getElementById("uno");
    let dos=document.getElementById("dos");
    let tres=document.getElementById("tres");
    let cuatro=document.getElementById("cuatro");
    let cinco=document.getElementById("cinco");
    let seis=document.getElementById("seis");
    let siete=document.getElementById("siete");
    let ocho=document.getElementById("ocho");
    let nueve=document.getElementById("nueve");
    let cero=document.getElementById("cero");
    let borrar=document.getElementById("borrar");
    let sumar=document.getElementById("sumar");
    let restar=document.getElementById("restar");
    let multiplicar=document.getElementById("multiplicar");
    let dividir=document.getElementById("dividir");


    //eventos para mostrar los numeros en pantalla 
    //relacionamos el evento con el numero  preguntar ahorita

    uno.onclick = function(e){
        resultado.textContent = resultado.textContent + "1";
    }
    dos.onclick = function(e){
        resultado.textContent = resultado.textContent+"2";
    }
    tres.onclick = function(e){
        resultado.textContent = resultado.textContent+"3";
    }
    cuatro.onclick = function(e){
        resultado.textContent = resultado.textContent+"4";
    }
    cinco.onclick = function(e){
        resultado.textContent = resultado.textContent+"5";
    }
    seis.onclick = function(e){
        resultado.textContent = resultado.textContent+"6";
    }
    siete.onclick = function(e){
        resultado.textContent = resultado.textContent+"7";
    }
    ocho.onclick = function(e){
        resultado.textContent = resultado.textContent+"8";
    }
    nueve.onclick = function(e){
        resultado.textContent = resultado.textContent+"9";
    }
    cero.onclick = function(e){
        resultado.textContent = resultado.textContent+"0";
    }

    borrar.onclick = function(e){
        operacion="C"
        borrar();
    }

    sumar.onclick = function(e){
        numa=resultado.textContent;
        operacion="+";
        limpiar();
    }
    restar.onclick = function(e){
        numa=resultado.textContent;
        operacion="-";
        limpiar();
    }
    multiplicar.onclick = function(e){
        numa=resultado.textContent;
        operacion="x";
        limpiar();
    }
    dividir.onclick=function(e){
        numa=resultado.textContent;
        operacion="÷";
        limpiar();
    }

    igual.onclick=function(e){
        numb=resultado.textContent;
        resolver();
    }
}

function limpiar(){
    resultado.textContent="";
}

function borrar(){
    resultado.textContent="";
    num1=0;
    num2=0;
    operacion="";
}
function resolver(){
    let res=0;
    switch(operacion){
        case "+":
            res=parseFloat(numa)+parseFloat(numb);
            break;
        case "-":
            res=parseFloat(numa)-parseFloat(numb);
            break;
        case "x":
            res=parseFloat(numa)*parseFloat(numb);
            break;
        case "÷":
            res=parseFloat(numa)/parseFloat(numb);
             break;
        
    }
    borrar();
    resultado.textContent=res;

}